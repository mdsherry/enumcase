extern crate proc_macro;
extern crate proc_macro2;

use heck::{CamelCase, KebabCase, SnakeCase, ShoutySnakeCase, MixedCase, TitleCase};
use proc_macro::{TokenStream};
use quote::quote;
use syn::{Meta, spanned::Spanned, self};

mod error;
use crate::error::error;

#[derive(Debug, Clone, Copy)]
enum Case {
    SnakeCase,
    CamelCase,
    KebabCase,
    LowerCase,
    UpperCase,
    ShoutySnakeCase,
    MixedCase,
    TitleCase
}

impl Case {
    // If only we had a `to_snake_case` method on the enum...
    fn name(self) -> &'static str {
        match self {
            Case::SnakeCase => "snake_case",
            Case::CamelCase => "camel_case",
            Case::KebabCase => "kebab_case",
            Case::LowerCase => "lower_case",
            Case::UpperCase => "upper_case",
            Case::ShoutySnakeCase => "shouty_snake_case",
            Case::MixedCase => "mixed_case",
            Case::TitleCase => "title_case"
        }
    }

    fn transform(self, s: &str) -> String {
        match self {
            Case::SnakeCase => s.to_snake_case(),
            Case::CamelCase => s.to_camel_case(),
            Case::KebabCase => s.to_kebab_case(),
            Case::LowerCase => s.to_camel_case().to_lowercase(),
            Case::UpperCase => s.to_camel_case().to_uppercase(),
            Case::ShoutySnakeCase => s.to_shouty_snake_case(),
            Case::MixedCase => s.to_mixed_case(),
            Case::TitleCase => s.to_title_case(),
        }
    }
}

fn get_cases(attrs: syn::AttributeArgs) -> (Vec<Case>, Vec<proc_macro2::TokenStream>) {
    let mut errors = vec![];
    let mut cases = vec![];
    for nm in attrs {
        match nm {
            syn::NestedMeta::Meta(Meta::Word(id)) => match id.to_string().to_snake_case().as_ref() {
                "snake_case" => cases.push(Case::SnakeCase),
                "camel_case" => cases.push(Case::CamelCase),
                "kebab_case" => cases.push(Case::KebabCase),
                "lowercase" => cases.push(Case::LowerCase),
                "uppercase" => cases.push(Case::UpperCase),
                "shouty_snake_case" => cases.push(Case::ShoutySnakeCase),
                "mixed_case" => cases.push(Case::MixedCase),
                "title_case" => cases.push(Case::TitleCase),
                _ => errors.push(error(&format!("Unrecognized case `{}`", id.to_string()), id.span(), id.span())),
            },
            syn::NestedMeta::Meta(v) => errors.push(error("Invalid syntax for handle_case", v.span(), v.span())),
            syn::NestedMeta::Literal(v) => errors.push(error("Don't use quotes around case names", v.span(), v.span())),
        }
    }
    (cases, errors)
}

#[proc_macro_attribute]
pub fn handle_case(attr: TokenStream, item: TokenStream) -> TokenStream {
    let attrs = syn::parse_macro_input!(attr as syn::AttributeArgs);
    let (cases, mut errors) = get_cases(attrs);

    let input = syn::parse_macro_input!(item as syn::ItemEnum);
    let ident = &input.ident;
    let mut idents = vec![];
    for variant in &input.variants {
        match variant.fields {
            syn::Fields::Unit => (),
            _ => errors.push(error("All enum variants must be unit types", variant.fields.span(), variant.fields.span()))
        };
        idents.push(&variant.ident);
    }

    let case_impls = cases.into_iter().map(|case| {
        let from_name = format!("from_{}", case.name());
        let from_name = syn::Ident::new(&from_name, proc_macro2::Span::call_site());

        let to_name = format!("to_{}", case.name());
        let to_name = syn::Ident::new(&to_name, proc_macro2::Span::call_site());

        let from_matches: Vec<_> = idents
            .iter()
            .map(|id| {
                let s = proc_macro2::Literal::string(&case.transform(&id.to_string()));
                quote! { #s => Ok(#ident::#id)}
            })
            .collect();

        let to_matches: Vec<_> = idents
            .iter()
            .map(|id| {
                let s = proc_macro2::Literal::string(&case.transform(&id.to_string()));
                quote! { #ident::#id => #s }
            })
            .collect();

        quote! {
            impl #ident {
                pub fn #from_name(s: &str) -> Result<Self, ()> {
                    match s {
                        #(#from_matches),* ,
                        _ => Err(())
                    }
                }

                pub fn #to_name(&self) -> &'static str {
                    match self {
                        #(#to_matches),*
                    }
                }
            }
        }
    });
    (quote! {
        #input
        #(#case_impls)*
        #(#errors)*
    })
    .into()
}

