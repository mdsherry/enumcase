# enumcase

TODO: Give everything better names

This adds a `handle_case` custom attribute that adds `from_` and `to_` methods for various casings of the names of enum variants.

For instance,
```
#[handle_case(snake_case, CamelCase)]
pub enum State {
    Playing,
    Paused,
    Stopped
}
```
will generate `to_snake_case`, `from_snake_case`, `to_camel_case` and `from_camel_case` methods on `State`.